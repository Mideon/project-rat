﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {
	public float jumpForce = 75;
	public float moveSpeed = 5;
	private float direction = 0;
	private float vdirection = 0;
	private Rigidbody2D rigidBody;

	private bool grounded;
	private bool isLadder;
	public Transform groundCheck;
	public LayerMask groundLayer;
	public LayerMask ladderLayer;
	public Transform ladderCheck;
	private float radiusCheck = 0.5f;


	// Use this for initialization
	void Start () {
	
	}

	void FixedUpdate() {
		grounded = Physics2D.OverlapCircle (groundCheck.position, radiusCheck, groundLayer);
		isLadder = Physics2D.OverlapCircle (ladderCheck.position, radiusCheck, ladderLayer);
	}

	// Update is called once per frame
	void Update () {
		rigidBody = GetComponent<Rigidbody2D> ();
		if (Input.GetButtonDown ("Fire1") && grounded) {
			rigidBody.velocity =  new Vector2( rigidBody.velocity.x, jumpForce );
		}
		direction = Input.GetAxisRaw ("Horizontal");

		vdirection = Input.GetAxisRaw ("Vertical");

		if(direction > 0) {
			rigidBody.velocity = new Vector2( moveSpeed, rigidBody.velocity.y );
			gameObject.transform.localScale = new Vector3(0.8412315f, 0.8412315f, 0.8412315f);
		}

		if (vdirection > 0 && isLadder) {
			rigidBody.velocity = new Vector2( rigidBody.velocity.x, 10 );
		}

		if (direction < 0) {
			rigidBody.velocity = new Vector2( -moveSpeed, rigidBody.velocity.y );
			gameObject.transform.localScale = new Vector3(-0.8412315f,0.8412315f,0.8412315f);
		}
	}
}
